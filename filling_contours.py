import numpy as np
import cv2
import os
from os import listdir
from os.path import isfile, join

folder_to_process = 'dataset1024/img_mask'
folder_to_store = 'dataset1024/img_processed'
images_paths = [f for f in listdir(folder_to_process) if isfile(join(folder_to_process, f))]

if '.DS_Store' in images_paths:
    images_paths.remove('.DS_Store')

print(images_paths)

# single
# img = cv2.imread(join(folder_to_process, images_paths[0]), 0)
# im2, contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# for i, cnt in enumerate(contours):
#     cv2.drawContours(img, contours, i, (255, 255, 255), -1)
#     cv2.imwrite(join(folder_to_store, images_paths[0]), img)
#     cv2.imshow("pic", img)
#     print("WTF")

for path in images_paths:
    print(path)
    img = cv2.imread(join(folder_to_process, path), 0)
    im2, contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for i, cnt in enumerate(contours):
        cv2.drawContours(img, contours, i, (255, 255, 255), -1)
        cv2.imwrite(join(folder_to_store, path), img)

k = cv2.waitKey(0)
if k == 27:         # wait for ESC key to exit
    cv2.destroyAllWindows()
